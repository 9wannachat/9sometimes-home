import React from 'react';
import './App.css';
import './asset/style/header.css';
import {Animated} from "react-animated-css";

function App() {
  return (
    <div className="App">
      <div className="img-header">
        <div className="center">
          <div>
            <h1 className="topic-wh" style={{marginBlockEnd: "0em"}}>Some<span>times</span></h1>
            <p className="sub-topic-wh" style={{marginBlockStart: "0em"}}>i'm developer welcome to my site</p>              
          </div>  
          <div style={{position: "absolute", bottom: "0", margin:"0 auto"}}>
            <p style={{fontSize: "8pt"}}>Image: https://pixabay.com/photos/norway-mountains-nature-mountain-4970081/</p>
          </div>         
        </div>          
      </div>    
    </div>
  );
}

export default App;
